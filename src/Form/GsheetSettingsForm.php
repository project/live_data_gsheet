<?php

namespace Drupal\live_data_gsheet\Form;

use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;

/**
 * Configure Google Sheet url settings for this.
 */
class GsheetSettingsForm extends ConfigFormBase {

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'gsheet_settings';
  }

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return ['live_data_gsheet.settings'];
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $form['gsheet_url'] = [
      '#type' => 'textarea',
      '#title' => $this->t('Deployment Id'),
      '#default_value' => $this->config('live_data_gsheet.settings')->get('gsheet_url'),
    ];

    return parent::buildForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $this->config('live_data_gsheet.settings')
      ->set('gsheet_url', $form_state->getValue('gsheet_url'))
      ->save();

    parent::submitForm($form, $form_state);
  }

}
