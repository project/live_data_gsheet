<?php
/**
 * @file
 * Contains \Drupal\live_data_gsheet\Controller\jsonController.
 */

namespace Drupal\live_data_gsheet\Controller;

use Drupal\Core\Controller\ControllerBase;

class LiveDataController extends ControllerBase {

  public function content() {
    $deployment_id = \Drupal::config('gsheet.settings')->get('gsheet_url');
    $response = file_get_contents("https://script.googleusercontent.com/macros/echo?user_content_key=".$deployment_id);
    $response = json_decode( $response, true );

    $result = $response;
    $data['header'] = $result[0];
    array_shift($result);
    $data['rows'] = $result;
    return array(
      '#theme' => 'gsheet',
      '#data' => $data,
      '#attached' => [
        'library' => ['live_data_gsheet/live_data_gsheet.data-table']
      ]
    );
  }
}

