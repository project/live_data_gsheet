(function ($) {
  var table = $('.rldfgs-table').DataTable({
    "columnDefs": [
      { "searchable": false, "targets": [1,2] },
      { "orderable": false, "targets": [1,2] },
    ],
    "order" : [[0, "asc"]],
  });
})(jQuery);
